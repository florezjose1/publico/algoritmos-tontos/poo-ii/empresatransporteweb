/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Data.EmpresaDAO;
import Model.EmpresaDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jose Flórez
 */
public class TestEmpresa {
    public static void main(String[] args) {
        EmpresaDAO e = new EmpresaDAO();
        String nit = String.valueOf((int)(Math.random() * 100));
        System.out.println("nit: " + nit);
        EmpresaDTO n = new EmpresaDTO("Empresa 1", nit, "empresa@gmail.com", "Clave123");
        
        // listar
        TestEmpresa.list(e);
        
        // Insertar
        TestEmpresa.create(e, n);
        
        // listar
        TestEmpresa.list(e);
        
        // Actualizar
        n.setNombre("Nueva nombre empresa");
        TestEmpresa.update(e, n);        
        
        // listar
        TestEmpresa.list(e);
       
        
        // Eliminar
        TestEmpresa.delete(e, n);
        
        // listar
        TestEmpresa.list(e);
    } 
    
    public static void list(EmpresaDAO e) {
        System.out.println("Empresas: ");
        List<EmpresaDTO> empresas = new ArrayList<>();
        empresas = e.getList();
        for(EmpresaDTO emp : empresas) {
            System.out.println(emp.toString());
        }
        System.out.println("================================================");
    }


    public static void create(EmpresaDAO e, EmpresaDTO n) {
        System.out.println("Insertando...");
        e.create(n);
    }
    
    public static void update(EmpresaDAO e, EmpresaDTO n) {
        System.out.println("Modificando...");
        e.update(n);
    }    
    
    public static void delete(EmpresaDAO e, EmpresaDTO n) {
        System.out.println("Eliminando...");
        e.delete(n);
    }    
}
