/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.CiudadDTO;
import Model.EmpresaDTO;
import Model.RutaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Flórez
 */
public class RutaDAO implements RutaServices {
    
    public static final String SQL_GET_ALL = "SELECT " + 
            "c1.codigo as codigo_ciudadorigen, c1.departamento as departament_ciudadorigen, c1.nombre as nombre_ciudadorigen, " +
            "c2.codigo as codigo_ciudaddestino, c2.departamento as departament_ciudaddestino, c2.nombre as nombre_ciudaddestino, " +
            "e.nombre as empresa, e.nit as nitempresa, e.correo as correoempresa, " +
            "r.codigo, r.tarifa, r.tiempoviaje " +
            "FROM `ruta` as r, empresa as e " +
            "INNER JOIN ciudad as c1 INNER JOIN ciudad as c2 " +
            "WHERE r.ciudadorigen = c1.codigo AND r.ciudaddestino = c2.codigo AND r.nitempresa = e.nit";
    
    public static final String SQL_ONE = "SELECT " + 
            "c1.codigo as codigo_ciudadorigen, c1.departamento as departament_ciudadorigen, c1.nombre as nombre_ciudadorigen, " +
            "c2.codigo as codigo_ciudaddestino, c2.departamento as departament_ciudaddestino, c2.nombre as nombre_ciudaddestino, " +
            "e.nombre as empresa, e.nit as nitempresa, e.correo as correoempresa, " +
            "r.codigo, r.tarifa, r.tiempoviaje " +
            "FROM `ruta` as r, empresa as e " +
            "INNER JOIN ciudad as c1 INNER JOIN ciudad as c2 " +
            "WHERE r.ciudadorigen = c1.codigo AND r.ciudaddestino = c2.codigo AND r.nitempresa = e.nit " +
            "AND r.codigo = ?";
    
    public static final String SQL_INSERT = "INSERT INTO `ruta`(`codigo`, `ciudadorigen`, `ciudaddestino`, `tarifa`, `nitempresa`, `tiempoviaje`) VALUES (?,?,?,?,?,?) ";
    public static final String SQL_UPDATE = "UPDATE ruta SET ciudadorigen = ?, ciudaddestino = ?, tarifa = ?, nitempresa = ?, tiempoviaje = ? WHERE codigo = ?";
    public static final String SQL_DELETE = "DELETE FROM ruta WHERE codigo = ?";
    
    @Override
    public List<RutaDTO> getList() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        RutaDTO ruta;
        List<RutaDTO> rutas = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_GET_ALL);
            res = ps.executeQuery();
            System.out.println("************************************************+");
            while (res.next()) {
                System.out.println("res: " + res.toString());
                int codigo = res.getInt("codigo");
                CiudadDTO ciudadorigen = new CiudadDTO(res.getString("nombre_ciudadorigen"), 
                                                       res.getString("codigo_ciudadorigen"), 
                                                       res.getString("departament_ciudadorigen"));
                CiudadDTO ciudaddestino = new CiudadDTO(res.getString("nombre_ciudaddestino"), 
                                                       res.getString("codigo_ciudaddestino"), 
                                                       res.getString("departament_ciudaddestino"));
                EmpresaDTO empresa = new EmpresaDTO(res.getString("empresa"), 
                                                    res.getString("nitempresa"),
                                                    res.getString("correoempresa"));
                int tarifa = res.getInt("tarifa");
                int tiempoviaje = res.getInt("tiempoviaje");
                
                ruta = new RutaDTO(codigo, ciudadorigen, ciudaddestino, tarifa, empresa, tiempoviaje);

                rutas.add(ruta);
                System.out.println("===================");
                System.out.println(rutas);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RutaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(RutaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return rutas;
    }
    
    public RutaDTO get(RutaDTO r) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_ONE);
            ps.setInt(1, r.getCodigo());
            res = ps.executeQuery();
            res.next();
            
            int codigo = res.getInt("codigo");
            CiudadDTO ciudadorigen = new CiudadDTO(res.getString("nombre_ciudadorigen"), 
                                                   res.getString("codigo_ciudadorigen"), 
                                                   res.getString("departament_ciudadorigen"));
            CiudadDTO ciudaddestino = new CiudadDTO(res.getString("nombre_ciudaddestino"), 
                                                   res.getString("codigo_ciudaddestino"), 
                                                   res.getString("departament_ciudaddestino"));
            EmpresaDTO empresa = new EmpresaDTO(res.getString("empresa"), 
                                                res.getString("nitempresa"),
                                                res.getString("correoempresa"));
            int tarifa = res.getInt("tarifa");
            int tiempoviaje = res.getInt("tiempoviaje");
                
            r = new RutaDTO(codigo, ciudadorigen, ciudaddestino, tarifa, empresa, tiempoviaje);

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return r;
    }
    
    @Override
    public int create(RutaDTO e) {
        return this.excecuteSQL(e, 3);
    }
    
    @Override
    public int update(RutaDTO e) {
        return this.excecuteSQL(e, 2);
    }
    
    @Override
    public int delete(RutaDTO e) {
        return this.excecuteSQL(e, 1);
    }
    
    private int excecuteSQL(RutaDTO e, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setInt(1, e.getCodigo());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, e.getCiudadOrigen().getCodigo());
                    ps.setString(2, e.getCiudadDestino().getCodigo());
                    ps.setInt(3, e.getTarifa());
                    ps.setString(4, e.getEmpresa().getNit());
                    ps.setInt(5, e.getTiempoViaje());
                    ps.setInt(6, e.getCodigo());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_INSERT);
                    ps.setInt(1, e.getCodigo());
                    ps.setString(2, e.getCiudadOrigen().getCodigo());
                    ps.setString(3, e.getCiudadDestino().getCodigo());
                    ps.setInt(4, e.getTarifa());
                    ps.setString(5, e.getEmpresa().getNit());
                    ps.setInt(6, e.getTiempoViaje());
                    
                    break;
                }
                default: break;
            }
            
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(RutaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(RutaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    } 

}
