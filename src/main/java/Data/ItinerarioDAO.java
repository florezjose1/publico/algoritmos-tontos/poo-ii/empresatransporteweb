/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.ItinerarioDTO;
import Model.ItinerarioKeyDTO;
import Model.PasajeroDTO;
import Model.RutaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Flórez - Andres Camperos
 */
public class ItinerarioDAO implements ItinerarioServices {
    // public static final String SQL_CONSULTA ="SELECT i.`id`, i.`pasajero_identificacion`, i.`itinerariokey_id`, k.id, k.fecha_partida, k.hora_partida, k.ruta_codigo, r.codigo, r.ciudadorigen, r.ciudaddestino, r.tarifa, e.nombre, p.nombre FROM `itinerario` as i, itinerariokey as k, ruta as r, empresa as e, pasajero as p WHERE i.itinerariokey_id = k.id and k.ruta_codigo = r.codigo and r.nitempresa = e.nit and i.pasajero_identificacion = p.identificacion";
    public static final String SQL_GET_ALL = String.join("SELECT * FROM `itinerario` as i, itinerariokey as k, ruta as r, empresa as e, pasajero as p", 
            "WHERE i.itinerariokey_id = k.id AND ", 
            "k.ruta_codigo = r.codigo AND ", 
            "r.nitempresa = e.nit AND ", 
            "i.pasajero_identificacion = p.identificacion"
            );
    private static final String SQL_CREATE = "INSERT INTO `itinerario` (`pasajero_cedula`, `itinerarioKey_id`) VALUES (?, ?)";
    private static final String SQL_UPDATE = "UPDATE `itinerario` SET `pasajero_identificacion`=?, `itinerariokey_id`=? WHERE id=?";
    private static final String SQL_DELETE = "DELETE FROM `itinerario` WHERE id=?";
    
    @Override
    public List<ItinerarioDTO> getList() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        ItinerarioDTO itinerario;
        List<ItinerarioDTO> itinerarios = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_GET_ALL);
            res = ps.executeQuery();
            while (res.next()) {
                PasajeroDTO pasajero = new PasajeroDTO(res.getString("nombre"), 
                                                       res.getString("correo"), 
                                                       res.getString("identificacion"));
                RutaDTO ruta = new RutaDTO(res.getInt("codigo"));
                ItinerarioKeyDTO itinerariokey = new ItinerarioKeyDTO(res.getInt("itinerariokey_id"),
                                                                      res.getString("fecha_partida"), 
                                                                      res.getString("hora_partida"),
                                                                      res.getString("fecha_llegada"), 
                                                                      res.getString("hora_llegada"),
                                                                      ruta);
                
                itinerario = new ItinerarioDTO(
                        res.getInt("id"),
                        pasajero,
                        itinerariokey
                );

                itinerarios.add(itinerario);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return itinerarios;
    }
    
    @Override
    public int create(ItinerarioDTO e) {
        return this.ejecutarSQL(e, 3);
    }

    @Override
    public int update(ItinerarioDTO e) {
        return this.ejecutarSQL(e, 2);
    }

    @Override
    public int delete(ItinerarioDTO e) {
        return this.ejecutarSQL(e, 1);
    } 
    
    private int ejecutarSQL(ItinerarioDTO e, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setInt(1, e.getId());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, e.getPasajero().getIdentificacion());
                    ps.setInt(2, e.getItinerarioKey().getId());
                    ps.setInt(3, e.getId());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_CREATE);
                    ps.setString(1, e.getPasajero().getIdentificacion());
                    ps.setInt(2, e.getItinerarioKey().getId());
                    break;
                }
                default: break;
            }
            
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
    
    
}
