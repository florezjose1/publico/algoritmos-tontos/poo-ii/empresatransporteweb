/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.EmpresaDTO;
import java.util.List;

/**
 *
 * @author flore
 */
public interface EmpresaServices {
    
    public List<EmpresaDTO> getList();
    public int create(EmpresaDTO e);
    public int update(EmpresaDTO e);
    public int delete(EmpresaDTO e);
    
}
