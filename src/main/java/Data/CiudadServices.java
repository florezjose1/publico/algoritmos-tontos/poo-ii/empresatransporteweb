/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.CiudadDTO;
import java.util.List;

/**
 *
 * @author Jose Andrés
 */
public interface CiudadServices {
    
    public List<CiudadDTO> getList();
    public int create(CiudadDTO e);
    public int update(CiudadDTO e);
    public int delete(CiudadDTO e);

}
