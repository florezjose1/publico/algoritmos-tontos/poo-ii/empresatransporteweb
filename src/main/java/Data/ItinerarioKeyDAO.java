/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.ItinerarioKeyDTO;
import Model.RutaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Flórez - Andres Camperos
 */
public class ItinerarioKeyDAO implements ItinerarioKeyServices {
    private static final String SQL_GET_ALL = "SELECT * FROM itinerarioKey";
    private static final String SQL_CREATE = "INSERT INTO `itinerarioKey`(`fecha_partida`, `hora_partida`,`fecha_llegada`, `hora_llegada`, `ruta_codigo`) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE `itinerarioKey` SET `fecha_partida`=?, `hora_partida`=?,`fecha_llegada`=?, `hora_llegada`=?, `ruta_codigo`=? WHERE id=?";
    private static final String SQL_DELETE = "DELETE FROM `itinerarioKey` WHERE id=?";
    
    @Override
    public List<ItinerarioKeyDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        ItinerarioKeyDTO itinerarioKey;
        List<ItinerarioKeyDTO> itinerariosKey = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_GET_ALL);
            res = ps.executeQuery();
            while (res.next()) {
                itinerarioKey = new ItinerarioKeyDTO(
                        res.getInt("id"),
                        res.getString("fecha_partida"),
                        res.getString("hora_partida"),
                        res.getString("fecha_llegada"),
                        res.getString("hora_llegada"),
                        new RutaDTO(res.getInt("ruta_codigo"))
                );

                itinerariosKey.add(itinerarioKey);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return itinerariosKey;
    }
    
    @Override
    public int crear(ItinerarioKeyDTO e) { 
        return this.ejecutarSQL(e, 3);
    }

    @Override
    public int actualizar(ItinerarioKeyDTO e) {
        return this.ejecutarSQL(e, 2);
    }
    
    @Override
    public int eliminar(ItinerarioKeyDTO e) {
        return this.ejecutarSQL(e, 1);
    } 
    
    private int ejecutarSQL(ItinerarioKeyDTO e, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setInt(1, e.getId());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, e.getFecha_partida());
                    ps.setString(2, e.getHora_partida());
                    ps.setString(3, e.getFecha_llegada());
                    ps.setString(4, e.getHora_llegada());
                    ps.setInt(5, e.getRuta().getCodigo());
                    ps.setInt(6, e.getId());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_CREATE);
                    ps.setString(1, e.getFecha_partida());
                    ps.setString(2, e.getHora_partida());
                    ps.setString(3, e.getFecha_llegada());
                    ps.setString(4, e.getHora_llegada());
                    ps.setInt(5, e.getRuta().getCodigo());
                    
                    break;
                }
                default: break;
            }
            
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(ItinerarioKeyDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(ItinerarioKeyDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
}
