/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.EmpresaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Flórez
 */
public class EmpresaDAO implements EmpresaServices {

    private static final String SQL_ALL = "SELECT nombre, nit, correo, clave FROM empresa";
    private static final String SQL_ONE = "SELECT nombre, nit, correo, clave FROM empresa WHERE nit = ?";
    private static final String SQL_INSERT = "INSERT INTO `empresa`(`nombre`, `nit`, `correo`, `clave`) VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE `empresa` SET `nombre`=?, `correo`=?,`clave`=? WHERE nit=?";
    private static final String SQL_DELETE = "DELETE FROM `empresa` WHERE nit=?";

    @Override
    public List<EmpresaDTO> getList() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        EmpresaDTO empresa;
        List<EmpresaDTO> empresas = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_ALL);
            res = ps.executeQuery();
            while (res.next()) {
                empresa = new EmpresaDTO(
                        res.getString("nombre"),
                        res.getString("nit"),
                        res.getString("correo"),
                        res.getString("clave")
                );

                empresas.add(empresa);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(EmpresaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return empresas;
    }

    public EmpresaDTO get(EmpresaDTO p) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_ONE);
            ps.setString(1, p.getNit());
            res = ps.executeQuery();
            res.next();
            String nombre = res.getString("nombre");
            String correo = res.getString("correo");
            String clave = res.getString("clave");
            p.setNombre(nombre);
            p.setCorreo(correo);
            p.setClave(clave);

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return p;
    }

    @Override
    public int create(EmpresaDTO e) {
        return this.excecuteSQL(e, 3);
    }

    @Override
    public int update(EmpresaDTO e) {
        return this.excecuteSQL(e, 2);
    }

    @Override
    public int delete(EmpresaDTO e) {
        return this.excecuteSQL(e, 1);
    }

    private int excecuteSQL(EmpresaDTO e, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setString(1, e.getNit());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, e.getNombre());
                    ps.setString(2, e.getCorreo());
                    ps.setString(3, e.getClave());
                    ps.setString(4, e.getNit());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_INSERT);
                    ps.setString(1, e.getNombre());
                    ps.setString(2, e.getNit());
                    ps.setString(3, e.getCorreo());
                    ps.setString(4, e.getClave());
                    break;
                }
                default:
                    break;
            }

            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EmpresaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(EmpresaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
}
