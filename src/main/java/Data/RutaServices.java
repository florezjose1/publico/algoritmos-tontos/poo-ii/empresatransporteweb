/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.RutaDTO;
import java.util.List;

/**
 *
 * @author Jose Flórez
 */
public interface RutaServices {
    
    public List<RutaDTO> getList();
    public int create(RutaDTO e);
    public int update(RutaDTO e);
    public int delete(RutaDTO e);

}
