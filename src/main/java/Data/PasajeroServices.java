/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.PasajeroDTO;
import java.util.List;

/**
 *
 * @author Neider Simarra
 */
public interface PasajeroServices {
    
    public List<PasajeroDTO> getList();
    public int create(PasajeroDTO e);
    public int update(PasajeroDTO e);
    public int delete(PasajeroDTO e);
    
}
