/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.CiudadDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Flórez
 */
public class CiudadDAO implements CiudadServices {
    private static final String SQL_GET_ALL = "SELECT nombre, codigo, departamento FROM ciudad";
    private static final String SQL_ONE = "SELECT nombre, codigo, departamento FROM ciudad WHERE codigo = ?";
    private static final String SQL_INSERT = "INSERT INTO `ciudad`(`nombre`, `codigo`, `departamento`) VALUES (?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE `ciudad` SET `nombre`=?, `departamento`=? WHERE codigo=?";
    private static final String SQL_DELETE = "DELETE FROM `ciudad` WHERE codigo=?";

    @Override
    public List<CiudadDTO> getList() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        CiudadDTO ciudad;
        List<CiudadDTO> ciudades = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_GET_ALL);
            res = ps.executeQuery();
            while (res.next()) {
                ciudad = new CiudadDTO(
                        res.getString("nombre"),
                        res.getString("codigo"),
                        res.getString("departamento")
                );

                ciudades.add(ciudad);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ciudades;
    }
    
    public CiudadDTO get(CiudadDTO p) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_ONE);
            ps.setString(1, p.getCodigo());
            res = ps.executeQuery();
            res.next();
            String nombre = res.getString("nombre");
            String correo = res.getString("departamento");
            p.setNombre(nombre);
            p.setDepartamento(correo);
            

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return p;
    }
    @Override
    public int create(CiudadDTO c) {
        return this.excecuteSQL(c, 3);
    }
    
    @Override
    public int update(CiudadDTO c) {
        return this.excecuteSQL(c, 2);
    }
    
    @Override
    public int delete(CiudadDTO c) {
        return this.excecuteSQL(c, 1);
    }
    
    private int excecuteSQL(CiudadDTO c, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setString(1, c.getCodigo());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, c.getNombre());
                    ps.setString(2, c.getDepartamento());
                    ps.setString(3, c.getCodigo());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_INSERT);
                    ps.setString(1, c.getNombre());
                    ps.setString(2, c.getCodigo());
                    ps.setString(3, c.getDepartamento());

                    break;
                }
                default: break;
            }
            
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
}
