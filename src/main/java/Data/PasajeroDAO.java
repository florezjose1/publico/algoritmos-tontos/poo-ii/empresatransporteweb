/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.PasajeroDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Neider Simarra
 */
public class PasajeroDAO implements PasajeroServices {

    public static final String SQL_GET_ALL = "SELECT nombre, correo, clave, identificacion FROM pasajero";
    public static final String SQL_ONE = "SELECT nombre, correo, clave, identificacion FROM pasajero WHERE identificacion = ?";
    public static final String SQL_INSERT = "iNSERT INTO pasajero (nombre, correo, clave, identificacion) VALUES (?,?,?,?) ";
    public static final String SQL_UPDATE = "UPDATE pasajero SET nombre = ?, correo = ?, clave = ? WHERE identificacion = ?";
    public static final String SQL_DELETE = "DELETE FROM pasajero WHERE identificacion = ?";

    @Override
    public List<PasajeroDTO> getList() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        PasajeroDTO pasajero;
        List<PasajeroDTO> pasajeros = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_GET_ALL);
            res = ps.executeQuery();
            while (res.next()) {
                pasajero = new PasajeroDTO(
                        res.getString("nombre"),
                        res.getString("correo"),
                        res.getString("clave"),
                        res.getString("identificacion")
                );

                pasajeros.add(pasajero);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PasajeroDTO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(PasajeroDTO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return pasajeros;
    }

    public PasajeroDTO get(PasajeroDTO p) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_ONE);
            ps.setString(1, p.getIdentificacion());
            res = ps.executeQuery();
            res.next();
            String nombre = res.getString("nombre");
            String correo = res.getString("correo");
            String clave = res.getString("clave");
            p.setNombre(nombre);
            p.setCorreo(correo);
            p.setClave(clave);

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return p;
    }
    
    @Override
    public int create(PasajeroDTO e) {
        return this.excecuteSQL(e, 3);
    }

    @Override
    public int update(PasajeroDTO e) {
        return this.excecuteSQL(e, 2);
    }

    @Override
    public int delete(PasajeroDTO e) {
        return this.excecuteSQL(e, 1);
    }

    private int excecuteSQL(PasajeroDTO e, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setString(1, e.getIdentificacion());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, e.getNombre());
                    ps.setString(2, e.getCorreo());
                    ps.setString(3, e.getClave());
                    ps.setString(4, e.getIdentificacion());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_INSERT);
                    ps.setString(1, e.getNombre());
                    ps.setString(2, e.getCorreo());
                    ps.setString(3, e.getClave());
                    ps.setString(4, e.getIdentificacion());
                    break;
                }
                default:
                    break;
            }

            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(PasajeroDTO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(PasajeroDTO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
}
