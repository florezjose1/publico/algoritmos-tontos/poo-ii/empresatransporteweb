/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.ItinerarioKeyDTO;
import java.util.List;

/**
 *
 * @author joseb
 */
public interface ItinerarioKeyServices {
     public List<ItinerarioKeyDTO> consultar();
    public int crear(ItinerarioKeyDTO e);
    public int actualizar(ItinerarioKeyDTO e);
    public int eliminar(ItinerarioKeyDTO e);
    
}
