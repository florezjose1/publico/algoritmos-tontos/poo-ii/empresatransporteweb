/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Jose Flórez - Andres Camperos
 */
public class ItinerarioDTO {
    private int id;
    private PasajeroDTO pasajero;
    private ItinerarioKeyDTO itinerario;

    public ItinerarioDTO() {
    }

    public ItinerarioDTO(int id, PasajeroDTO pasajero, ItinerarioKeyDTO itinerario) {
        this.id = id;
        this.pasajero = pasajero;
        this.itinerario = itinerario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PasajeroDTO getPasajero() {
        return pasajero;
    }

    public void setPasajero(PasajeroDTO pasajero) {
        this.pasajero = pasajero;
    }

    public ItinerarioKeyDTO getItinerarioKey() {
        return itinerario;
    }

    public void setItinerarioKey(ItinerarioKeyDTO itinerario) {
        this.itinerario = itinerario;
    }

    @Override
    public String toString() {
        return "ItinerarioDTO{" + "id=" + this.id + ", pasajero_cedula=" + this.pasajero.toString() + ", itinerario=" + this.itinerario.toString() + '}';
    }
    
}
