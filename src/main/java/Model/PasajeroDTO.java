/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Neider Simarra
 */
public class PasajeroDTO {
    private String nombre;
    private String Correo;
    private String clave;
    private String identificacion;

    public PasajeroDTO() {
    }

    public PasajeroDTO(String nombre, String Correo, String identificacion) {
        this.nombre = nombre;
        this.Correo = Correo;
        this.identificacion = identificacion;
    }

    public PasajeroDTO(String nombre, String Correo, String clave, String identificacion) {
        this.nombre = nombre;
        this.Correo = Correo;
        this.clave = clave;
        this.identificacion = identificacion;
    }

    public PasajeroDTO(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    @Override
    public String toString() {
        return "PasajeroDTO{" + "nombre=" + this.nombre + ", Correo=" + this.Correo + ", clave=" + this.clave + ", identificacion=" + this.identificacion + '}';
    } 
}
