/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author Jose Flórez
 */
public class CiudadDTO {
    private String nombre;
    private String codigo;
    private String departamento;

    public CiudadDTO() {
    }

    public CiudadDTO(String nombre, String codigo, String departamento) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.departamento = departamento;
    }

    public CiudadDTO(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CiudadDTO other = (CiudadDTO) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ciudad{" + "nombre=" + this.nombre + ", codigo=" + this.codigo + ", departamento=" + this.departamento + '}';
    }
}
