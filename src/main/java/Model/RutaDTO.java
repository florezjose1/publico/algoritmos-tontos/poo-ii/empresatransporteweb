/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author Jose Flórez
 */
public class RutaDTO {
    private int codigo;
    private CiudadDTO ciudadOrigen;
    private CiudadDTO ciudadDestino;
    private int tarifa;
    private EmpresaDTO empresa;
    private int tiempoViaje;

    public RutaDTO() {
    }

    public RutaDTO(int codigo) {
        this.codigo = codigo;
    }
    
    public RutaDTO(int codigo, CiudadDTO ciudadorigen, CiudadDTO ciudaddestino, int tarifa, EmpresaDTO empresa, int tiempoviaje) {
        this.codigo = codigo;
        this.ciudadOrigen = ciudadorigen;
        this.ciudadDestino = ciudaddestino;
        this.tarifa = tarifa;
        this.empresa = empresa;
        this.tiempoViaje = tiempoviaje;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public CiudadDTO getCiudadOrigen() {
        return ciudadOrigen;
    }

    public void setCiudadOrigen(CiudadDTO ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }

    public CiudadDTO getCiudadDestino() {
        return ciudadDestino;
    }

    public void setCiudadDestino(CiudadDTO ciudadDestino) {
        this.ciudadDestino = ciudadDestino;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public EmpresaDTO getEmpresa() {
        return empresa;
    }

    public void setEmpresa(EmpresaDTO empresa) {
        this.empresa = empresa;
    }

    public int getTiempoViaje() {
        return tiempoViaje;
    }

    public void setTiempoViaje(int tiempoViaje) {
        this.tiempoViaje = tiempoViaje;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RutaDTO other = (RutaDTO) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RutaDTO{" + "codigo=" + codigo + ", ciudadorigen=" + ciudadOrigen.toString() + ", ciudaddestino=" + ciudadDestino.toString() + ", tarifa=" + tarifa + ", empresa=" + empresa.toString() + ", tiempoviaje=" + tiempoViaje + '}';
    }
    
}
