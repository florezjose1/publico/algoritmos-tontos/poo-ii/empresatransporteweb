/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author Jose Flórez
 */
public class EmpresaDTO {
    private String nombre;
    private String nit;
    private String correo;
    private String clave;

    public EmpresaDTO() {
    }

    public EmpresaDTO(String nombre, String nit, String correo) {
        this.nombre = nombre;
        this.nit = nit;
        this.correo = correo;
    }
    
    public EmpresaDTO(String nit) {
        this.nit = nit;
    }

    public EmpresaDTO(String nombre, String nit, String correo, String clave) {
        this.nombre = nombre;
        this.nit = nit;
        this.correo = correo;
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpresaDTO other = (EmpresaDTO) obj;
        if (!Objects.equals(this.nit, other.nit)) {
            return false;
        }
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Empresa{" + "nombre=" + this.nombre + ", nit=" + this.nit + ", correo=" + this.correo + ", clave=" + this.clave + '}';
    }
    
}
