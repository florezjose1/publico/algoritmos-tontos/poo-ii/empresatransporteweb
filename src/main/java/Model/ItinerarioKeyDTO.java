/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Jose Flórez - Andres Camperos
 */
public class ItinerarioKeyDTO {
    private int id;
    private String fecha_partida;
    private String hora_partida;
    private String fecha_llegada;
    private String hora_llegada;
    private RutaDTO ruta;

    public ItinerarioKeyDTO() {
    }

    public ItinerarioKeyDTO(int id, String fecha_partida, String hora_partida, String fecha_llegada, String hora_llegada, RutaDTO ruta) {
        this.id = id;
        this.fecha_partida = fecha_partida;
        this.hora_partida = hora_partida;
        this.fecha_llegada = fecha_llegada;
        this.hora_llegada = hora_llegada;
        this.ruta = ruta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha_partida() {
        return fecha_partida;
    }

    public void setFecha_partida(String fecha_partida) {
        this.fecha_partida = fecha_partida;
    }

    public String getHora_partida() {
        return hora_partida;
    }

    public void setHora_partida(String hora_partida) {
        this.hora_partida = hora_partida;
    }

    public String getFecha_llegada() {
        return fecha_llegada;
    }

    public void setFecha_llegada(String fecha_llegada) {
        this.fecha_llegada = fecha_llegada;
    }

    public String getHora_llegada() {
        return hora_llegada;
    }

    public void setHora_llegada(String hora_llegada) {
        this.hora_llegada = hora_llegada;
    }

    public RutaDTO getRuta() {
        return this.ruta;
    }

    public void setHora_llegada(RutaDTO ruta) {
        this.ruta = ruta;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItinerarioKeyDTO other = (ItinerarioKeyDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItinerarioKeyDTO{" + "id=" + this.id + ", fecha_partida=" + this.fecha_partida + 
                ", hora_partida=" + this.hora_partida + ", fecha_llegada=" + this.fecha_llegada + 
                ", hora_llegada=" + this.hora_llegada + ", ruta=" + this.ruta.toString() + '}';
    }
    
    
    
}
