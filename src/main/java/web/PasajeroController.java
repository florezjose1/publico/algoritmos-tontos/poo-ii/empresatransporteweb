/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Data.PasajeroDAO;
import Model.PasajeroDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Andres Camperos
 */
@WebServlet("/pasajeros")
public class PasajeroController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        System.out.println("A: " + action);
        if (action != null) {
            switch (action) {
                case "edit": {
                    this.editPasajero(req, resp);
                    break;
                }
                case "delete": {
                    this.deletePasajero(req, resp);
                    break;
                }

                default: {
                    this.getListPasajeros(req, resp);
                    break;
                }
            }
        } else {
            this.getListPasajeros(req, resp);
        }
    }

    private void editPasajero(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String identificacion = req.getParameter("identificacion");

        PasajeroDTO pasajero = new PasajeroDTO(identificacion);
        PasajeroDTO e = new PasajeroDAO().get(pasajero);
        req.setAttribute("pasajero", e);
        req.getRequestDispatcher("editPasajero.jsp").forward(req, resp);
    }

    private void deletePasajero(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String identificacion = req.getParameter("identificacion");
        PasajeroDTO n = new PasajeroDTO(identificacion);
        new PasajeroDAO().delete(n);
        this.getListPasajeros(req, resp);
    }

    private void getListPasajeros(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PasajeroDAO pasajero = new PasajeroDAO();
        List<PasajeroDTO> pasajeros = pasajero.getList();

        req.setAttribute("pasajeros", pasajeros);
        HttpSession sesion = req.getSession();
        sesion.setAttribute("pasajeros", pasajeros);

        // resp.sendRedirect("pasajeros.jsp");
        req.getRequestDispatcher("pasajeros.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add": {
                this.addPasajero(req, resp);
                break;
            }
            case "update": {
                this.updatePasajero(req, resp);
                break;
            }

            default: {
                this.getListPasajeros(req, resp);
                break;
            }

        }
    }

    private void addPasajero(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nombre = req.getParameter("nombre");
        String identificacion = req.getParameter("identificacion");
        String correo = req.getParameter("correo");
        String clave = req.getParameter("clave");

        if (!nombre.equals("") && !identificacion.equals("") && !correo.equals("") && !clave.equals("")) {
            PasajeroDAO e = new PasajeroDAO();
            PasajeroDTO n = new PasajeroDTO(nombre, correo, clave, identificacion);
            e.create(n);
        }
        this.getListPasajeros(req, resp);
    }

    private void updatePasajero(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String identificacion = req.getParameter("identificacion");
        String nombre = req.getParameter("nombre");
        String correo = req.getParameter("correo");
        String clave = req.getParameter("clave");

        PasajeroDTO pasajero = new PasajeroDTO(nombre, correo, clave, identificacion);
        System.out.println("Pasajero: " + pasajero);
        new PasajeroDAO().update(pasajero);

        this.getListPasajeros(req, resp);

    }
}
