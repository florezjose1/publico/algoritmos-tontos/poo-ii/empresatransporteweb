/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Data.CiudadDAO;
import Data.EmpresaDAO;
import Data.RutaDAO;
import Model.CiudadDTO;
import Model.EmpresaDTO;
import Model.RutaDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jose Flórez
 */
@WebServlet("/rutas")
public class RutaController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        
        if(action!=null) {
            switch (action) {
                case "edit": {
                    this.editRuta(req, resp);
                    break;
                }
                case "delete": {
                    this.deleteRuta(req, resp);
                    break;
                }

                default: {
                    this.getListRutas(req, resp);
                    break;
                }
            }
        } else {
            this.getListRutas(req, resp);
        }
    }

    private void editRuta(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String codigo = req.getParameter("codigo");

        RutaDTO ruta = new RutaDTO(Integer.parseInt(codigo));
        RutaDTO e = new RutaDAO().get(ruta);
        req.setAttribute("ruta", e);
        
        CiudadDAO ciudad = new CiudadDAO();
        List<CiudadDTO> ciudades = ciudad.getList();
        req.setAttribute("ciudades", ciudades);
        
        EmpresaDAO empresa = new EmpresaDAO();
        List<EmpresaDTO> empresas = empresa.getList();
        req.setAttribute("empresas", empresas);
        
        req.getRequestDispatcher("editRuta.jsp").forward(req, resp);
    }

    private void deleteRuta(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String codigo = req.getParameter("codigo");
        RutaDTO n = new RutaDTO(Integer.parseInt(codigo));
        new RutaDAO().delete(n);
        this.getListRutas(req, resp);
    }

    private void getListRutas(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RutaDAO ruta = new RutaDAO();
        List<RutaDTO> rutas = ruta.getList();
        req.setAttribute("rutas", rutas);
        
        CiudadDAO ciudad = new CiudadDAO();
        List<CiudadDTO> ciudades = ciudad.getList();
        req.setAttribute("ciudades", ciudades);
        
        EmpresaDAO empresa = new EmpresaDAO();
        List<EmpresaDTO> empresas = empresa.getList();
        req.setAttribute("empresas", empresas);

        req.getRequestDispatcher("rutas.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add": {
                this.addRuta(req, resp);
                break;
            }
            case "update": {
                this.updateRuta(req, resp);
                break;
            }

            default: {
                this.getListRutas(req, resp);
                break;
            }

        }
    }

    private void addRuta(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String codigo = req.getParameter("codigo");
        String ciudad_o = req.getParameter("ciudad_origen");
        String ciudad_d = req.getParameter("ciudad_destino");
        String tarifa = req.getParameter("tarifa");
        String nitempresa = req.getParameter("nitempresa");
        String tiempo = req.getParameter("tiempoviaje");

        if (!codigo.equals("") && !ciudad_o.equals("") && !ciudad_d.equals("") && !tarifa.equals("")
                && !nitempresa.equals("") && !tiempo.equals("")) {
            RutaDAO r = new RutaDAO();
            CiudadDTO c_o = new CiudadDTO(ciudad_o);
            CiudadDTO c_d = new CiudadDTO(ciudad_d);
            EmpresaDTO e = new EmpresaDTO(nitempresa);
            
            RutaDTO n = new RutaDTO(Integer.parseInt(codigo), c_o, c_d, Integer.parseInt(tarifa), e, Integer.parseInt(tiempo));
            r.create(n);
        }
        this.getListRutas(req, resp);
    }
    
    private void updateRuta(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String codigo = req.getParameter("codigo");
        String ciudad_o = req.getParameter("ciudad_origen");
        String ciudad_d = req.getParameter("ciudad_destino");
        String tarifa = req.getParameter("tarifa");
        String nitempresa = req.getParameter("nitempresa");
        String tiempo = req.getParameter("tiempoviaje");

        if (!codigo.equals("") && !ciudad_o.equals("") && !ciudad_d.equals("") && !tarifa.equals("")
                && !nitempresa.equals("") && !tiempo.equals("")) {
            RutaDAO r = new RutaDAO();
            CiudadDTO c_o = new CiudadDTO(ciudad_o);
            CiudadDTO c_d = new CiudadDTO(ciudad_d);
            EmpresaDTO e = new EmpresaDTO(nitempresa);
            
            RutaDTO n = new RutaDTO(Integer.parseInt(codigo), c_o, c_d, Integer.parseInt(tarifa), e, Integer.parseInt(tiempo));
            r.update(n);
        }
        this.getListRutas(req, resp);

    }

}
