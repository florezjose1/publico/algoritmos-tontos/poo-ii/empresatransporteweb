/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Data.EmpresaDAO;
import Model.EmpresaDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jose Flórez
 */
@WebServlet("/empresas")
public class EmpresaController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        System.out.println("A: " + action);
        if(action!=null) {
            switch (action) {
                case "edit": {
                    this.editEmpresa(req, resp);
                    break;
                }
                case "delete": {
                    this.deleteEmpresa(req, resp);
                    break;
                }

                default: {
                    this.getListEmpresas(req, resp);
                    break;
                }
            }
        } else {
            this.getListEmpresas(req, resp);
        }
    }

    private void editEmpresa(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nit = req.getParameter("nit");

        EmpresaDTO empresa = new EmpresaDTO(nit);
        EmpresaDTO e = new EmpresaDAO().get(empresa);
        req.setAttribute("empresa", e);
        req.getRequestDispatcher("editEmpresa.jsp").forward(req, resp);
    }

    private void deleteEmpresa(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nit = req.getParameter("nit");
        EmpresaDTO n = new EmpresaDTO(nit);
        new EmpresaDAO().delete(n);
        this.getListEmpresas(req, resp);
    }

    private void getListEmpresas(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EmpresaDAO empresa = new EmpresaDAO();
        List<EmpresaDTO> empresas = empresa.getList();

        req.setAttribute("empresas", empresas);
        HttpSession sesion = req.getSession();
        sesion.setAttribute("empresas", empresas);

        // resp.sendRedirect("empresas.jsp");

        req.getRequestDispatcher("empresas.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add": {
                this.addEmpresa(req, resp);
                break;
            }
            case "update": {
                this.updateEmpresa(req, resp);
                break;
            }

            default: {
                this.getListEmpresas(req, resp);
                break;
            }

        }
    }

    private void addEmpresa(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nombre = req.getParameter("nombre");
        String nit = req.getParameter("nit");
        String correo = req.getParameter("correo");
        String clave = req.getParameter("clave");

        if (!nombre.equals("") && !nit.equals("") && !correo.equals("") && !clave.equals("")) {
            EmpresaDAO e = new EmpresaDAO();
            EmpresaDTO n = new EmpresaDTO(nombre, nit, correo, clave);
            e.create(n);
        }
        this.getListEmpresas(req, resp);
    }
    
    private void updateEmpresa(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nit = req.getParameter("nit");
        String nombre = req.getParameter("nombre");
        String correo = req.getParameter("correo");
        String clave = req.getParameter("clave");

        EmpresaDTO empresa = new EmpresaDTO(nombre, nit, correo, clave);
        System.out.println("Empresa: " + empresa);
        new EmpresaDAO().update(empresa);
        
        this.getListEmpresas(req, resp);

    }

}
