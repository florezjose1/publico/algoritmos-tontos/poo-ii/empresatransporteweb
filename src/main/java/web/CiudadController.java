/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import Data.CiudadDAO;
import Model.CiudadDTO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jose Flórez
 */
@WebServlet("/ciudades")
public class CiudadController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        System.out.println("A: " + action);
        if (action != null) {
            switch (action) {
                case "edit": {
                    this.editCiudad(req, resp);
                    break;
                }
                case "delete": {
                    this.deleteCiudad(req, resp);
                    break;
                }

                default: {
                    this.getListCiudades(req, resp);
                    break;
                }
            }
        } else {
            this.getListCiudades(req, resp);
        }
    }

    private void editCiudad(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String codigo = req.getParameter("codigo");

        CiudadDTO ciudad = new CiudadDTO(codigo);
        CiudadDTO e = new CiudadDAO().get(ciudad);
        req.setAttribute("ciudad", e);
        req.getRequestDispatcher("editCiudad.jsp").forward(req, resp);
    }

    private void deleteCiudad(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String codigo = req.getParameter("codigo");
        CiudadDTO n = new CiudadDTO(codigo);
        new CiudadDAO().delete(n);
        this.getListCiudades(req, resp);
    }

    private void getListCiudades(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CiudadDAO ciudad = new CiudadDAO();
        List<CiudadDTO> ciudades = ciudad.getList();

        req.setAttribute("ciudades", ciudades);
        HttpSession sesion = req.getSession();
        sesion.setAttribute("ciudades", ciudades);

        // resp.sendRedirect("ciudades.jsp");
        req.getRequestDispatcher("ciudades.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add": {
                this.addCiudad(req, resp);
                break;
            }
            case "update": {
                this.updateCiudad(req, resp);
                break;
            }

            default: {
                this.getListCiudades(req, resp);
                break;
            }

        }
    }

    private void addCiudad(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nombre = req.getParameter("nombre");
        String codigo = req.getParameter("codigo");
        String departamento = req.getParameter("departamento");

        if (!nombre.equals("") && !codigo.equals("") && !departamento.equals("")) {
            CiudadDAO e = new CiudadDAO();
            CiudadDTO n = new CiudadDTO(nombre, codigo, departamento);
            e.create(n);
        }
        this.getListCiudades(req, resp);
    }

    private void updateCiudad(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nombre = req.getParameter("nombre");
        String codigo = req.getParameter("codigo");
        String departamento = req.getParameter("departamento");

        CiudadDTO ciudad = new CiudadDTO(nombre, codigo, departamento);
        System.out.println("Ciudad: " + ciudad);
        new CiudadDAO().update(ciudad);

        this.getListCiudades(req, resp);

    }
}
