<%-- 
    Document   : pasajeros
    Created on : 21/12/2020, 09:02:00 PM
    Author     : flore
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pasajeros</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <style>
            .content {
                position: relative;
            }

            .content .btn-floating {
                position: absolute;
                right: -29px;
                top: -29px;
            }
            
            .back {
                color: black;
                top: -42px;
                position: absolute;
            }
        </style>


        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h4 class="center">Pasajeros</h4>
            <div class="content">
                <a class="btn-floating btn-large waves-effect waves-light red modal-trigger" href="#modal_add">
                    <i class="material-icons">add</i>
                </a>
                <a class="back" href="/empresatransporteweb/index.jsp"><i class="material-icons">keyboard_backspace</i></a>
                
                <table class="responsive-table striped highlight centered z-depth-5">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>correo</th>
                            <th>Identificación</th>
                            <th>Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="p" items="${pasajeros}">
                            <tr>
                                <td>${p.nombre}</td>
                                <td>${p.correo}</td>
                                <td>${p.identificacion}</td>
                                <td>
                                    <a href="?action=edit&identificacion=${p.identificacion}" class="btn-flat blue-text"><i class="material-icons">visibility</i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

            <!-- Modal Structure -->
            <form class="col s12" method="post" action="?action=add">
                <div id="modal_add" class="modal">
                    <div class="modal-content">
                        <h6>Agregar Pasajero</h6> <br>
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input name="identificacion" id="identificacion" type="text" class="validate"required>
                                        <label for="identificacion">Identificación</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input name="nombre" id="first_name" type="text" class="validate" required>
                                        <label for="first_name">Nombre</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input name="correo" id="correo" type="email" class="validate" required>
                                        <label for="correo">Correo</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input name="clave" id="clave" type="password" class="validate" required>
                                        <label for="clave">Clave</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                        <button type="submit" class="waves-effect waves-green btn-flat">Agregar</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems, {});
        });
    </script>
</html>
