<%-- 
    Document   : ciudades
    Created on : 10/12/2020, 08:31:19 AM
    Author     : Jose Flórez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ciudades</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <style>
            .content {
                position: relative;
            }

            .content .btn-floating {
                position: absolute;
                right: -29px;
                top: -29px;
            }
            
            .back {
                color: black;
                top: -42px;
                position: absolute;
            }
        </style>


        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h4 class="center">Ciudades</h4>
            <div class="content">
                <a class="btn-floating btn-large waves-effect waves-light red modal-trigger" href="#modal_add">
                    <i class="material-icons">add</i>
                </a>
                <a class="back" href="/empresatransporteweb/index.jsp"><i class="material-icons">keyboard_backspace</i></a>
                <table class="responsive-table striped highlight centered z-depth-5">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Departamento</th>
                            <th>Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="c" items="${ciudades}">
                            <tr>
                                <td>${c.codigo}</td>
                                <td>${c.nombre}</td>
                                <td>${c.departamento}</td>
                                <td>
                                    <a href="?action=edit&codigo=${c.codigo}" class="btn-flat blue-text"><i class="material-icons">visibility</i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>


            <!-- Modal Structure -->
            <form class="col s12" method="post" action="?action=add">
                <div id="modal_add" class="modal">
                    <div class="modal-content">
                        <h6>Agregar ciudad</h6> <br>
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input name="codigo" id="codigo" type="text" class="validate" required>
                                        <label for="codigo">Código</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input name="nombre" id="first_name" type="text" class="validate" required>
                                        <label for="first_name">Nombre</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input name="departamento" id="departamento" type="text" class="validate"required>
                                        <label for="departamento">Departamento</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
                        <button type="submit" class="waves-effect waves-green btn-flat">Agregar</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems, {});
        });
    </script>
</html>
