<%-- 
    Document   : index
    Created on : 6/12/2020, 04:02:45 PM
    Author     : flore
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <style>
            .container {
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
            }

            .card {
                width: 250px;
                height: 150px;
                margin: 20px;
                display: flex;
                align-items: center;
                justify-content: center;
                display: flex;
                flex-direction: column;
                flex-wrap: wrap;
            }
            
            .card:hover {
                transform: scale(1.1);
                transition: all 0.2s ease-in-out 0s;
                box-shadow: 0 4px 8px rgba(134, 151, 168, 0.5);
                transform: translateY(-3px);
            }
            
            .card .title {
                margin: 0;
                font-size: 1.5rem;
            }
            
            .card i {
                font-size: 3.5rem;
                margin: 10px auto;
            }
        </style>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>
        <div class="container">
            <a href="${pageContext.request.contextPath}/ciudades">
                <div class="card z-depth-5">
                    <i class="material-icons">gamepad</i>
                    <h3 class="title">Ciudad</h3>
                </div>
            </a>
            <a href="${pageContext.request.contextPath}/pasajeros">
                <div class="card z-depth-5">
                    <i class="material-icons">emoji_people</i>
                    <h3 class="title">Pasajeros</h3>
                </div>
            </a>
            <a href="${pageContext.request.contextPath}/empresas">
                <div class="card z-depth-5">
                    <i class="material-icons">account_balance</i>
                    <h3 class="title">Empresas</h3>
                </div>
            </a>
            <a href="${pageContext.request.contextPath}/rutas">
                <div class="card z-depth-5">
                    <i class="material-icons">moving</i>
                    <h3 class="title">Rutas</h3>
                </div>
            </a>
        </div>
    </body>
</html>
