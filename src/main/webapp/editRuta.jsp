<%-- 
    Document   : editRuta
    Created on : 16/12/2020, 09:37:34 PM
    Author     : Jose Flórez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Empresa</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <style>
            .action {
                display: flex;
                justify-content: space-between;
            }
        </style>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h4 class="center">Editar Ruta ${ruta.ciudadOrigen.nombre} - ${ruta.ciudadDestino.nombre}</h4>
            <div class="content">
                <form method="post" action="?action=update">
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="hidden" name="codigo" value="${ruta.codigo}">
                            <input name="codigo_" id="codigo_" type="text" class="validate" value="${ruta.codigo}" required disabled>
                            <label for="codigo_">Código</label>
                        </div>
                        <div class="input-field col s6">
                            <select name="nitempresa">
                                <option value="" disabled selected>Seleccione una empresa</option>

                                <c:forEach var="e" items="${empresas}">
                                    <c:choose>
                                        <c:when test="${ruta.empresa.nit == e.nit}">
                                            <option value="${e.nit}" selected>${e.nombre}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${e.nit}">${e.nombre}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <label>Empresa</label>
                        </div>
                        <div class="input-field col s6">
                            <select name="ciudad_origen">
                                <option value="" disabled selected>Seleccione una ciudad origen</option>
                                <c:forEach var="c" items="${ciudades}">
                                    <c:choose>
                                        <c:when test="${ruta.ciudadOrigen.codigo == c.codigo}">
                                            <option value="${c.codigo}" selected>${c.nombre}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${c.codigo}">${c.nombre}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <label>Ciudad Origen</label>
                        </div>
                        <div class="input-field col s6">
                            <select name="ciudad_destino">
                                <option value="" disabled selected>Seleccione una ciudad destino</option>
                                <c:forEach var="c" items="${ciudades}">
                                    <c:choose>
                                        <c:when test="${ruta.ciudadDestino.codigo == c.codigo}">
                                            <option value="${c.codigo}" selected>${c.nombre}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${c.codigo}">${c.nombre}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <label>Ciudad Destino</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="tarifa" id="tarifa" type="number" class="validate" value="${ruta.tarifa}" required>
                            <label for="tarifa">Tarifa</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="tiempoviaje" id="tiempoviaje" type="number" class="validate" value="${ruta.tiempoViaje}" required>
                            <label for="tiempoviaje">Tiempo viaje</label>
                        </div>
                        <div class="col s12 action">
                            <a href="?action=delete&codigo=${ruta.codigo}" class="btn red">Delete</a>
                            <div>
                                <a href="${pageContext.request.contextPath}/rutas" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
                                <button type="submit" class="waves-effect waves-green btn">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
    <script>
        document.addEventListener('DOMContentLoaded', function () {

            var selects = document.querySelectorAll('select');
            var instances_selects = M.FormSelect.init(selects, {});
            
        });
    </script>
</html>
