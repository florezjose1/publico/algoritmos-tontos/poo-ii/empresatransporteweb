<%-- 
    Document   : editEmpresa
    Created on : 16/12/2020, 09:37:34 PM
    Author     : flore
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Empresa</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        
        <style>
            .action {
                display: flex;
                justify-content: space-between;
            }
        </style>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h4 class="center">Editar ciudad ${ciudad.nombre}</h4>
            <div class="content">
                <form method="post" action="?action=update">
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="hidden" name="codigo" value="${ciudad.codigo}">
                            <input name="codigo_" id="codigo_" type="text" class="validate" value="${ciudad.codigo}" required disabled>
                            <label for="codigo_">Código</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="nombre" id="nombre" type="text" class="validate" value="${ciudad.nombre}" required>
                            <label for="nombre">Nombre</label>
                        </div>
                        <div class="input-field col s12">
                            <input name="departamento" id="departamento" type="text" class="validate" value="${ciudad.departamento}" required>
                            <label for="departamento">Departamento</label>
                        </div>
                        <div class="col s12 action">
                            <a href="?action=delete&codigo=${ciudad.codigo}" class="btn red">Delete</a>
                            <div>
                                <a href="${pageContext.request.contextPath}/ciudades" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
                                <button type="submit" class="waves-effect waves-green btn">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
