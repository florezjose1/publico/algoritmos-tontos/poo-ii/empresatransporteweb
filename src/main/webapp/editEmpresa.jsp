<%-- 
    Document   : editEmpresa
    Created on : 16/12/2020, 09:37:34 PM
    Author     : flore
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Empresa</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        
        <style>
            .action {
                display: flex;
                justify-content: space-between;
            }
        </style>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h4 class="center">Editar empresa ${empresa.nombre}</h4>
            <div class="content">
                <form method="post" action="?action=update">
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="hidden" name="nit" value="${empresa.nit}">
                            <input name="nit_" id="nit_" type="text" class="validate" value="${empresa.nit}" required disabled>
                            <label for="nit_">NIT</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="nombre" id="first_name" type="text" class="validate" value="${empresa.nombre}" required>
                            <label for="first_name">Nombre</label>
                        </div>
                        <div class="input-field col s12">
                            <input name="correo" id="correo" type="email" class="validate" value="${empresa.correo}" required>
                            <label for="correo">Correo</label>
                        </div>
                        <div class="input-field col s12">
                            <input name="clave" id="clave" type="password" class="validate" value="${empresa.clave}" required>
                            <label for="clave">Clave</label>
                        </div>
                        <div class="col s12 action">
                            <a href="?action=delete&nit=${empresa.nit}" class="btn red">Delete</a>
                            <div>
                                <a href="${pageContext.request.contextPath}/empresas" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
                                <button type="submit" class="waves-effect waves-green btn">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
