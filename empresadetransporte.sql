-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-12-2020 a las 15:17:25
-- Versión del servidor: 10.2.10-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empresadetransporte`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `nombre` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `codigo` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `departamento` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`nombre`, `codigo`, `departamento`) VALUES
('Cúcuta', '540001', 'Norte de Santander'),
('Bucaramanga', '540002', 'Santander'),
('Bogota', '900000', 'Cundinamarca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `nombre` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nit` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `correo` varchar(30) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `clave` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`nombre`, `nit`, `correo`, `clave`) VALUES
('Rappi Jose', '56', 'rappijose@gmail.co', '1234'),
('Peralonso', '90001', 'peralonso@gmail.com', 'Peralonso2'),
('Copetran', '91001', 'copetran@gmail.com', 'Copetran20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itinerario`
--

CREATE TABLE `itinerario` (
  `id` int(11) NOT NULL,
  `pasajero_identificacion` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `itinerariokey_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `itinerario`
--

INSERT INTO `itinerario` (`id`, `pasajero_identificacion`, `itinerariokey_id`) VALUES
(1, '1000001', 2),
(2, '200001', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itinerariokey`
--

CREATE TABLE `itinerariokey` (
  `id` int(11) NOT NULL,
  `fecha_partida` date NOT NULL,
  `hora_partida` time NOT NULL,
  `fecha_llegada` date NOT NULL,
  `hora_llegada` time NOT NULL,
  `ruta_codigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `itinerariokey`
--

INSERT INTO `itinerariokey` (`id`, `fecha_partida`, `hora_partida`, `fecha_llegada`, `hora_llegada`, `ruta_codigo`) VALUES
(2, '2020-11-29', '26:56:57', '2020-11-30', '30:56:57', 540003),
(3, '2020-12-01', '10:48:32', '2020-12-01', '14:48:32', 540004);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajero`
--

CREATE TABLE `pasajero` (
  `identificacion` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `clave` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pasajero`
--

INSERT INTO `pasajero` (`identificacion`, `nombre`, `correo`, `clave`) VALUES
('1000001', 'Jose', 'jose@gmail.com', '123'),
('200001', 'Andrés', 'andres@gmail.com', 'andres123'),
('902930230', 'Neider', 'neider@gmail.com', 'neider');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta`
--

CREATE TABLE `ruta` (
  `codigo` int(11) NOT NULL,
  `ciudadorigen` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ciudaddestino` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tarifa` int(11) NOT NULL,
  `nitempresa` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tiempoviaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `ruta`
--

INSERT INTO `ruta` (`codigo`, `ciudadorigen`, `ciudaddestino`, `tarifa`, `nitempresa`, `tiempoviaje`) VALUES
(9011, '540001', '900000', 200000, '56', 540),
(540003, '540002', '540001', 25000, '56', 500),
(540004, '540001', '540002', 50000, '90001', 2000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`nit`);

--
-- Indices de la tabla `itinerario`
--
ALTER TABLE `itinerario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pasajero_identificacion` (`pasajero_identificacion`),
  ADD KEY `itinerario_key` (`itinerariokey_id`);

--
-- Indices de la tabla `itinerariokey`
--
ALTER TABLE `itinerariokey`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ruta_codigo` (`ruta_codigo`);

--
-- Indices de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD PRIMARY KEY (`identificacion`);

--
-- Indices de la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `ruta_empresa` (`nitempresa`),
  ADD KEY `ruta_ciudadorigen` (`ciudadorigen`),
  ADD KEY `ruta_ciudaddestino` (`ciudaddestino`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `itinerario`
--
ALTER TABLE `itinerario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `itinerariokey`
--
ALTER TABLE `itinerariokey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ruta`
--
ALTER TABLE `ruta`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=540005;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `itinerario`
--
ALTER TABLE `itinerario`
  ADD CONSTRAINT `itinerario_key` FOREIGN KEY (`itinerariokey_id`) REFERENCES `itinerariokey` (`id`),
  ADD CONSTRAINT `pasajero_identificacion` FOREIGN KEY (`pasajero_identificacion`) REFERENCES `pasajero` (`identificacion`);

--
-- Filtros para la tabla `itinerariokey`
--
ALTER TABLE `itinerariokey`
  ADD CONSTRAINT `ruta_codigo` FOREIGN KEY (`ruta_codigo`) REFERENCES `ruta` (`codigo`);

--
-- Filtros para la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD CONSTRAINT `ruta_ciudaddestino` FOREIGN KEY (`ciudaddestino`) REFERENCES `ciudad` (`codigo`),
  ADD CONSTRAINT `ruta_ciudadorigen` FOREIGN KEY (`ciudadorigen`) REFERENCES `ciudad` (`codigo`),
  ADD CONSTRAINT `ruta_empresa` FOREIGN KEY (`nitempresa`) REFERENCES `empresa` (`nit`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
